# Server - user functions

Here you can find all of the different user related functionality that the file [server.py](../../../src/app/server.py) makes available to the client. Within each JSON body, you must include a `"op"` value in order to specify the operation you want the server to perform. All properties are required unless a property is specified to be optional.

## Table of Contents

1. [add_user](#add_user)
2. [remove_user](#remove_user)
3. [recover_user](#recover_user)
4. [get_user_info](#get_user_info)
5. [get_all_users](#get_all_users)
6. [filter_users](#filter_users)
7. [log_in_user](#log_in_user)
8. [hard_delete_user](#hard_delete_user)
9. [decrement_next_user_id](#decrement_next_user_id)

---

## add_user

### Description

Creates a new user object according to the information given (provided that it's valid) and stores it. Once the user is created, the function returns the new user object (ID included).

### Example JSON body

``` json
{
    "op": "add_user",
    "user_model": {
        "name": "Barry B. Benson",
        "email": "barry@bmail.com",
        "username": "be-more-bee",
        "password": "you.like.jazz",
        "is_admin": false
    }
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| user_model | Object | Encapsulates the information for the soon-to-be-created user.
| name | String | The full name of the user.
| email | String | The user's email.
| username | String | The user's chosen username (*must be unique*).
| password | String | The user's password.
| is_admin | Boolean | `True` if the user is an admin, `False` otherwise.

<br />

## remove_user

### Description

Marks a user with the given ID as "removed" instead of *actually* deleting a user from the data base. **Only admins or the user him/herself can remove a user**.

### Example JSON body

``` json
{
    "op": "add_user",
    "user_id": 3
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| user_id | Number | The ID of the user to remove.

<br />

## recover_user

### Description

Restores a user's account that has formerly been removed. **This functionality is only available to administrators!** If a user wants to restore his/her former account then he/she should get in touch with Bookflip's support team to recover the account.

### Example JSON body

``` json
{
    "op": "recover_user",
    "user_id": 3
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| user_id | Number | The ID of the user that needs to be recovered.

<br />

## get_user_info

### Description

Returns a user's information relative to the given user ID. Can return multiple versions of a user's info, for example if requesting the info of a different user, you get a very basic overview whereas if a user requests his/her own info, he/she gets a more detailed set of information.

### Example JSON body

``` json
{
    "op": "get_user_info",
    "user_id": 3
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| user_id | Number | The ID of the user to get information about.

<br />

## get_all_users

### Description

Returns a list of all registered users within the system, even the deleted ones. **Only admins should have an access to this functionality**.

### Example JSON body

``` json
{
    "op": "get_all_users"
}
```

### Property descriptions

There are no extra properties to define.

<br />

## filter_users

### Description

Returns a list of user information where the users conform to a certain search criteria. **This should only be available to admins**. If you don't want to search by a certain property then you can include it, but leave it empty.

### Example JSON body

``` json
{
    "op": "filter_users",
    "search_model": {
        "name": "Barry B. Benson",
        "email": "barry@bmail.com",
        "username": "be-more-bee"
    }
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| user_id | Number | The ID of the user to get information about.

<br />

## log_in_user

### Description

Logs in a specified user and returns a session ID (*as a string, not an object*) that the user can use to authenticate himself later.

### Example JSON body

``` json
{
    "op": "log_in_user",
    "user_id": 3
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| user_id | Number | The ID of the user to log in.

<br />

## hard_delete_user

> **WARNING**: This method should only be used in test files, not in production.

### Description

Instead of just marking a user as deleted, this method *actually* deletes a user from the system, so be careful with it.

### Example JSON body

``` json
{
    "op": "hard_delete_user",
    "user_id": 2
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| user_id | Number | The ID of the user to delete.

<br />

## decrement_next_user_id

> **WARNING**: This method should only be used in test files, not in production.

### Description

Decrements the next available ID for users, should only be used in test files.

### Example JSON body

``` json
{
    "op": "decrement_next_user_id"
}
```

### Property descriptions

There are no extra properties to describe.