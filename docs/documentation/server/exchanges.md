# Server - exchange functions

Here you can find all of the different exchange related functionality that the file [server.py](../../../src/app/server.py) makes available to the client. Within each JSON body, you must include a `"op"` value in order to specify the operation you want the server to perform. All properties are required unless a property is specified to be optional.

## Table of Contents

1. [create_exchange](#create_exchange)
2. [remove_exchange](#remove_exchange)
3. [get_all_exchanges](#get_all_exchanges)
4. [get_exchange_info](#get_exchange_info)
5. [hard_delete_exchange](#hard_delete_exchange)
6. [decrement_next_exchange_id](#decrement_next_exchange_id)

---

## create_exchange

### Description

Creates a new exchange object according to the information given (provided that it's valid) and stores it. Once the exchange is created, the function returns the new exchange object (ID included).

### Example JSON body

``` json
{
    "op": "create_exchange",
    "exchange_model": {
        "userid1": 2,
        "userid2": 3,
        "bookid1": 1,
        "bookid2": 5
    }
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| exchange_model | Object | Holds all relevant information regarding the to-be-created exchange.
| userid1 | Number | The ID of one of the two users participating in the exchange.
| userid2 | Number | The ID of the other one of the two users participating in the exchange.
| bookid1 | Number | The ID of *userid1*'s book.
| bookid2 | Number | The ID of *userid2*'s book.

<br />

## remove_exchange

### Description

Marks an exchange as *removed* so that it won't appear in search results and so on.

### Example JSON body

``` json
{
    "op": "remove_exchange",
    "exchange_id": 3
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| exchange_id | Number | The ID of the exchange to remove.

<br />

## get_all_exchanges

### Description

Returns a dictionary populated with all the exchanges registered within the system.

### Example JSON body

``` json
{
    "op": "get_all_exchanges"
}
```

### Property descriptions

There are no properties to describe.

<br />

## get_exchange_info

### Description

Returns the exchange registered within the system that has the given exchange ID.

### Example JSON body

``` json
{
    "op": "get_exchange_info",
    "exchange_id": 2
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| exchange_id | Number | The ID of the exchange to get info about.

<br />

## hard_delete_exchange

> **WARNING**: This method should only be used in test files, not in production.

### Description

Instead of just marking an exchange as deleted, this method *actually* deletes an exchange from this system, so be careful with this.

### Example JSON body

``` json
{
    "op": "hard_delete_exchange",
    "exchange_id": 1
}
```

### Property descriptions

| Name        | Type   | Description
|-------------|--------|------------
| exchange_id | Number | The ID of the exchange that is to be deleted forever.

<br />

## decrement_next_exchange_id

> **WARNING**: This method should only be used in test files, not in production.

### Description

Lowers the next available exchange ID by 1, don't use this method for anything else than in test files.

### Example JSON body

``` json
{
    "op": "decrement_next_exchange_id"
}
```

### Property descriptions

There are no additional properties to describe.