from app.repositories.exchange_repository import ExchangeRepository

""" A class for encapsulating the functionality of the exchange service. """
class ExchangeService(object):
    """ The constructor for 'ExchangeService'. """
    def __init__(self):
        self.__exchange_repository = ExchangeRepository()
    """ Returns a list of exchanges. """
    def get_all_exchanges(self):
        return self.__exchange_repository.get_all_exchanges()

    """ Returns an exchane with the given ID. """
    def get_exchange(self, exchange_id):
        return self.__exchange_repository.get_exchange(exchange_id)

    """ Returns the next avaliable ID. """
    def get_next_exchange_id(self):
        return self.__exchange_repository.get_next_exchange_id()

    """ Creates a new exchange object and passes it down to the repository. """
    def create_exchange(self, exchange_dict):
        self.__validate_exchange_dict(exchange_dict)
        exchange_dict["id"] = self.get_next_exchange_id()
        exchange_dict["status"] = 1
        new_exchange = self.__exchange_repository.add_exchange(exchange_dict)
        return new_exchange

    """ Sets an exchange with the given ID to deleted. """
    def delete_exchange(self, exchange_id):
        self.__exchange_repository.delete_exchange(exchange_id)
        
    """ Deletes an exchange with the given ID from file. """
    def hard_delete_exchange(self, exchange_id):
        if isinstance(exchange_id, int) == False:
            return "Invalid exchange ID for hard delete"
        if exchange_id > self.__exchange_repository.get_next_exchange_id():
            return "Exchange ID out of bounds for hard delete"
        else:
            self.__exchange_repository.hard_delete_exchange(exchange_id)
    
    """ Decrements the next available exchange ID (only for testing purposes) """
    def decrement_next_exchange_id(self):
        self.__exchange_repository.decrement_next_exchange_id()

    """ Checks if an exchange dictionary is valid """
    def __validate_exchange_dict(self, exchange_dict):
        if exchange_dict == None:
            raise ValueError("Some required information was not given")
        has_valid_properties, message = self.__check_exchange_dict_properties(exchange_dict)
        if not has_valid_properties:
            raise ValueError(message)
        has_valid_types, message = self.__check_exchange_dict_value_types(exchange_dict)
        if not has_valid_types:
            raise ValueError(message)
    
    """ Checks if an exchange dictionary has only the valid properties """
    def __check_exchange_dict_properties(self, exchange_dict):
        properties = ["userid1", "userid2", "bookid1", "bookid2"]
        for prop in properties:
            if prop not in exchange_dict:
                return (False, "Model is missing property: {}".format(prop))
        if len(exchange_dict.keys()) != len(properties):
            return (False, "Model includes some unwanted properties")
        return (True, "All is well")
    
    """ Checks if an exchange dictionary has all the valid value types """
    def __check_exchange_dict_value_types(self, exchange_dict):
        if  type(exchange_dict["userid1"]) != int or \
            type(exchange_dict["userid2"]) != int or \
            type(exchange_dict["bookid1"]) != int or \
            type(exchange_dict["bookid2"]) != int:
            return (False, "Some property/properties are not of the correct type")
        return (True, "All is well")