from app.repositories.rental_repository import RentalRepository

""" A class that encapsulates the functionality of the rental service."""
class RentalService(object):
    """ The constructor for 'RentalService'."""
    def __init__(self):
        self.__rental_repository = RentalRepository()

    """ Creates a new rental object and passes it down to the repository."""
    def create_rental(self, rental_dict):
        self.__validate_rental_dict(rental_dict)
        rental_dict["id"] = self.__rental_repository.get_next_rental_id()
        rental_dict["status"] = 1
        new_rental = self.__rental_repository.add_rental(rental_dict)
        return new_rental
    
    """
        Checks if a rental with the given ID exists, then passes the rental down
        to the rental repository.
    """
    def close_rental(self, rental_id):
        if not self.__rental_repository.rental_exists(rental_id):
            raise ValueError(f"Cannot close rental with ID={rental_id} since it does not exist")
        self.__rental_repository.close_rental(rental_id)

    """ Returns a rental with the given ID."""
    def get_rental(self, rental_id):
        if not self.__rental_repository.rental_exists(rental_id):
            raise ValueError(f"Cannot get rental with ID={rental_id} since it does not exist")
        return self.__rental_repository.get_rental(rental_id)

    """ Returns all rentals."""
    def get_all_rentals(self):
        return self.__rental_repository.get_all_rentals()

    """ Returns the next avalible ID."""
    def get_next_rental_id(self):
        return self.__rental_repository.get_next_rental_id()
    
    """ Permanently deletes a rental from the system"""
    def hard_delete_rental(self, rental_id):
        if isinstance(rental_id, int) == False:
            return "Invalid rental ID for hard delete"
        if not self.__rental_repository.rental_exists(rental_id):
            raise ValueError(f"Cannot hard delete a rental with ID={rental_id}, it doesn't exist")
        self.__rental_repository.hard_delete_rental(rental_id)
    
    """ Decrement netx rental ID. (For testing purpose only) """
    def decrement_next_rental_id(self):
        self.__rental_repository.decrement_next_rental_id()

    """ Validates a rental dictionary."""
    def __validate_rental(self, rental_dict):
        if 'lender_id' not in rental_dict:
            return False
        if 'book_id' not in rental_dict:
            return False
        if 'renter_id' not in rental_dict:
            return False
        return True
    
    """ Checks if an rental dictionary is valid """
    def __validate_rental_dict(self, rental_dict):
        if rental_dict == None:
            raise ValueError("Some required information was not given")
        has_valid_properties, message = self.__check_rental_dict_properties(rental_dict)
        if not has_valid_properties:
            raise ValueError(message)
        has_valid_types, message = self.__check_rental_dict_value_types(rental_dict)
        if not has_valid_types:
            raise ValueError(message)
    
    """ Checks if an rental dictionary has only the valid properties """
    def __check_rental_dict_properties(self, rental_dict):
        properties = ["lender_id", "book_id", "renter_id"]
        for prop in properties:
            if prop not in rental_dict:
                return (False, "Model is missing property: {}".format(prop))
        if len(rental_dict.keys()) != len(properties):
            return (False, "Model includes some unwanted properties")
        return (True, "All is well")
    
    """ Checks if an rental dictionary has all the valid value types """
    def __check_rental_dict_value_types(self, rental_dict):
        if  type(rental_dict["lender_id"]) != int or \
            type(rental_dict["book_id"]) != int or \
            type(rental_dict["renter_id"]) != int:
            return (False, "Some property/properties are not of the correct type")
        return (True, "All is well")