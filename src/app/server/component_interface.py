import json
from app.services.book_service import BookService
from app.services.user_service import UserService
from app.services.exchange_service import ExchangeService
from app.services.message_service import MessageService
from app.services.rental_service import RentalService

"""
    This is the actual interface of the server, containing the methods we
    expose to the clients. If you wish to see how the requests for each of these
    "endpoints" should look like, then you can check out the documentation
    located in the docs/documentation folder.
"""
class ComponentInterface(object):
    """ The constructor for the component interface """
    def __init__(self):
        self.__book_service = BookService()
        self.__user_service = UserService()
        self.__exchange_service = ExchangeService()
        self.__message_service = MessageService()
        self.__rental_service = RentalService()

    """ Registers a new book within the system and stores it in bookdata.json """
    def add_book(self, request_dict):
        new_book = self.__book_service.add_book(request_dict["book_model"])
        return json.dumps(new_book)
    
    """ Marks a book with the given ID as removed """
    def remove_book(self, request_dict):
        self.__book_service.remove_book(int(request_dict["book_id"]))
        return ""
    
    """ Returns information about the book with the given ID """
    def get_book_info(self, request_dict):
        book = self.__book_service.get_book_by_id(request_dict["book_id"])
        return json.dumps(book)
    
    """ Fully updates a book with the given ID with the information given """
    def update_book(self, request_dict):
        book_id = request_dict["book_id"]
        session_id = request_dict["session_id"]
        book_model = request_dict["book_model"]
        updated_book = self.__book_service.update_book(book_id, session_id, book_model)
        return json.dumps(updated_book)
    
    """ Returns all books that conform to a certain search criteria """
    def filter_books(self, request_dict):
        search_model = request_dict["search_model"]
        filtered_books = self.__book_service.filter_books(search_model)
        return json.dumps(filtered_books)
    
    """ Returns all books stored in the system """
    def get_all_books(self, request_dict):
        all_books = self.__book_service.get_all_books()
        return json.dumps(all_books)
    
    """ Marks the book with the given ID as unavailable """
    def make_book_unavailable(self, request_dict):
        book_id = int(request_dict["book_id"])
        self.__book_service.change_book_availability(book_id, 2)
        return ""
    
    """ Marks the book with the given ID as available """
    def make_book_available(self, request_dict):
        book_id = int(request_dict["book_id"])
        self.__book_service.change_book_availability(book_id, 1)
        return ""
    
    """ Permanently removes a book from the system (only use in test files) """
    def hard_delete_book(self, request_dict):
        self.__book_service.hard_delete_book(int(request_dict["book_id"]))
        return ""

    """ Decrements the next available book ID for new books (only use in test files) """
    def decrement_next_book_id(self, request_dict):
        self.__book_service.decrement_next_book_id()
        return ""

    """ Creates a new exchange and stores it """
    def create_exchange(self, request_dict):
        exchange_model = request_dict["exchange_model"]
        new_exchange = self.__exchange_service.create_exchange(exchange_model)
        return json.dumps(new_exchange)
    
    """ Marks an exchange with the specified ID as removed """
    def remove_exchange(self, request_dict):
        exchange_id = request_dict["exchange_id"]
        self.__exchange_service.delete_exchange(exchange_id)
        return ""
    
    """ Returns all exchanges stored in the system """
    def get_all_exchanges(self, request_dict):
        all_exchanges = self.__exchange_service.get_all_exchanges()
        return json.dumps(all_exchanges)
    
    """ Returns information about the exchange with the specified ID """
    def get_exchange_info(self, request_dict):
        exchange_id = request_dict["exchange_id"]
        exchange = self.__exchange_service.get_exchange(exchange_id)
        return json.dumps(exchange)
    
    """ Permanently removes an exchange with the given ID from the system (only use in test files) """
    def hard_delete_exchange(self, request_dict):
        exchange_id = int(request_dict["exchange_id"])
        self.__exchange_service.hard_delete_exchange(exchange_id)
        return ""
    
    """ Decrements the next available ID for new exchanges (only use in test files) """
    def decrement_next_exchange_id(self, request_dict):
        self.__exchange_service.decrement_next_exchange_id()
        return ""
    
    """ Creates and stores a new message in the system """
    def send_message(self, request_dict):
        message_model = request_dict["message_model"]
        new_message = self.__message_service.send_message(message_model)
        return json.dumps(new_message)
    
    """ Gets all messages that have been sent to a user with the given ID """
    def get_messages_to(self, request_dict):
        user_id = request_dict["user_id"]
        messages = self.__message_service.get_messages_to(user_id)
        return json.dumps(messages)
    
    """ Gets all messages that were written by the user with the given ID """
    def get_messages_by(self, request_dict):
        user_id = request_dict["user_id"]
        messages = self.__message_service.get_messages_by(user_id)
        return json.dumps(messages)
    
    """ Gets all messages that userid1 and userid2 either wrote or received """
    def get_messages_between(self, request_dict):
        userid1 = request_dict["userid1"]
        userid2 = request_dict["userid2"]
        messages = self.__message_service.get_messages_between(userid1, userid2)
        return json.dumps(messages)
    
    """ Gets information about the message with the specified message ID """
    def get_message_info(self, request_dict):
        message_id = request_dict["message_id"]
        message = self.__message_service.get_message(message_id)
        return json.dumps(message)
    
    """ Returns all the messages stored in the system """
    def get_all_messages(self, request_dict):
        all_messages = self.__message_service.get_all_messages()
        return json.dumps(all_messages)
    
    """ Permanently deletes a message from the system (only use in test files) """
    def hard_delete_message(self, request_dict):
        message_id = request_dict["message_id"]
        self.__message_service.hard_delete_message(message_id)
        return ""
    
    """ Decrements the next available message ID for new messages (only use in test files) """
    def decrement_next_message_id(self, request_dict):
        self.__message_service.decrement_next_message_id()
        return ""
    
    """ Creates and stores a new rental in rentaldata.json """
    def create_rental(self, request_dict):
        rental_model = request_dict["rental_model"]
        new_rental = self.__rental_service.create_rental(rental_model)
        return json.dumps(new_rental)
    
    """ Closes the rental with the specified ID """
    def close_rental(self, request_dict):
        rental_id = request_dict["rental_id"]
        self.__rental_service.close_rental(rental_id)
        return ""
    
    """ Gets and returns all rentals registered in the system. """
    def get_all_rentals(self, request_dict):
        all_rentals = self.__rental_service.get_all_rentals()
        return json.dumps(all_rentals)
    
    """ Gets information about a certain rental with the given rental ID """
    def get_rental_info(self, request_dict):
        rental_id = request_dict["rental_id"]
        rental = self.__rental_service.get_rental(rental_id)
        return json.dumps(rental)
    
    """ Permanently deletes a rental with the given rental ID (only use in test files) """
    def hard_delete_rental(self, request_dict):
        rental_id = request_dict["rental_id"]
        self.__rental_service.hard_delete_rental(rental_id)
        return ""

    """ Decrements the next available rental ID for new rentals (only use in test files) """
    def decrement_next_rental_id(self, request_dict):
        self.__rental_service.decrement_next_rental_id()
        return ""
    
    """ Creates a new user and stores him/her in the system """
    def add_user(self, request_dict):
        user_model = request_dict["user_model"]
        new_user = self.__user_service.add_user(user_model)
        return json.dumps(new_user)
    
    """ Removes (marks as removed) the user with the specified user ID """
    def remove_user(self, request_dict):
        user_id = request_dict["user_id"]
        self.__user_service.remove_user(user_id)
        return ""
    
    """ Marks a formerly deleted user as available again """
    def recover_user(self, request_dict):
        user_id = request_dict["user_id"]
        self.__user_service.recover_user(user_id)
        return ""

    """ Gets information about a specified user with the given user ID """
    def get_user_info(self, request_dict):
        user_id = request_dict["user_id"]
        user = self.__user_service.get_user(user_id)
        return json.dumps(user)
    
    """ Returns all the users registered in the system """
    def get_all_users(self, request_dict):
        all_users = self.__user_service.get_all_users()
        return json.dumps(all_users)
    
    """ Returns only the users that meet a certain search criteria """
    def filter_users(self, request_dict):
        search_model = request_dict["search_model"]
        users = self.__user_service.filter_users(search_model)
        return json.dumps(users)
    
    """ Logs in a user and returns the user's session ID (string) """
    def log_in_user(self, request_dict):
        user_id = request_dict["user_id"]
        session_id = self.__user_service.log_in_user(user_id)
        return session_id
    
    """ Permanently deletes a user from the system (only use in test files) """
    def hard_delete_user(self, request_dict):
        user_id = request_dict["user_id"]
        self.__user_service.hard_delete_user(user_id)
        return ""
    
    """ Decrements the next available user ID (only use in test files) """
    def decrement_next_user_id(self, request_dict):
        self.__user_service.decrement_next_user_id()
        return ""