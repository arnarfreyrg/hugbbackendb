import asyncio
import json
import websockets
import os
from app.server.component_interface import ComponentInterface

"""
    This class represents our technical connection to the outside (process
    incoming text and so on). It is currently implemented as a singleton class
    but we might change that later on.
"""
class ComponentPort(object):
    __instance = None

    """ This method is static => you won't need a class instance to run it """
    @staticmethod
    def get_instance():
        if not ComponentPort.__instance:
            ComponentPort()
        return ComponentPort.__instance
    
    def __init__(self):
        if ComponentPort.__instance:
            raise Exception("This class is a singleton!")
        else:
            ComponentPort.__instance = self
            self.interface = ComponentInterface()
    
    """ Handles messages coming from the client. """
    async def __message_handler(self, websocket, path):
        message = await websocket.recv()
        data = json.loads(message)
        return_value = "The request body needs to have an 'op' property"
        if "op" in data:
            try:
                return_value = getattr(self.interface, data["op"])(data)
            except AttributeError:
                return_value = "Tried to call a server function that is not implemented"
        await websocket.send(return_value)
    """ Starts the server. """
    def start(self):
        start_server = \
            websockets.serve(self.__message_handler, "0.0.0.0", os.environ.get("PORT", 8080))
        asyncio.get_event_loop().run_until_complete(start_server)
        asyncio.get_event_loop().run_forever()

if __name__ == "__main__":
    component_port = ComponentPort.get_instance()
    component_port.start()